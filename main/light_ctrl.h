#ifndef __LIGHT_CTRL_H__
#define __LIGHT_CTRL_H__

#include <string.h>

struct rgb_t {
    uint8_t red;
    uint8_t green;
    uint8_t blue;
    uint8_t intensity;
};


void set_led(bool on, struct rgb_t color);

void configure_led(void);

#endif  // __LIGHT_CTRL_H__
