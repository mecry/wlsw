#ifndef __WLSW_H__
#define __WLSW_H__


#include "button.h"
#include "light_ctrl.h"
#include "espnow_interface.h"


enum WlswEventType_e {
    WLSW_BUTTON_PRESS,
    WLSW_ESPNOW_MSG_RCV,
    WLSW_ELEMENT_SIZE,
};

typedef struct {
    enum WlswEventType_e type;
    union {
        button_event_t btn_event;
        espnow_mevent_t espnow_event;
    } data;
} WlswEvent_t;

void notify_button_event(button_event_t* btn_event);
void notify_espnow_event(espnow_mevent_t* espnow_event);
esp_err_t wslw_init();

#endif  // __WLSW_H__
