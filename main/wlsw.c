#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#include <stdio.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "esp_log.h"
#include "sdkconfig.h"

#include "wlsw.h"


#define TAG "wlsw"


QueueHandle_t queue;
int mMainTaskQuit = 0;

static WlswEvent_t devent = {};


static void handle_btn_press(const button_event_t* btn_event)
{
    static bool on = true;
    static uint8_t after_long = 0;
    struct rgb_t color = { .red = 8, .green = 16, .blue = 0};
    struct espnow_msg_t msg = { };

    switch (btn_event->event) {
        case BUTTON_UP:
            break;
        case BUTTON_DOWN:
            on = !on;
            set_led(on, color);

            if (btn_event->pin == CONFIG_GPIO_INPUT_0) {
                msg.type = ESPNOW_BUTTON1_SHORT;
            } else if (btn_event->pin == CONFIG_GPIO_INPUT_1) {
                msg.type = ESPNOW_BUTTON2_SHORT;
            } else {
                ESP_LOGE(TAG, "Unknown pin");
                break;
            }
            espnow_send_message(&msg);
            break;
        case BUTTON_HELD:
            if (after_long > 0) {
                on = !on;
                set_led(on, color);
            }
            after_long += 1;

            if (btn_event->pin == CONFIG_GPIO_INPUT_0) {
                msg.type = ESPNOW_BUTTON1_LONG;
            } else if (btn_event->pin == CONFIG_GPIO_INPUT_1) {
                msg.type = ESPNOW_BUTTON2_LONG;
            } else {
                ESP_LOGE(TAG, "Unknown pin");
                break;
            }
            espnow_send_message(&msg);
            break;
        default:
            ESP_LOGE(TAG, "Unhandled btn event %d encountered", btn_event->event);
    }
}

static void handle_espnow_rcv(const espnow_mevent_t* event)
{
    struct rgb_t color = {};
    switch (event->type) {
        case ESPNOW_BUTTON1_SHORT:
            color.red = 64,
            set_led(true, color);
            break;
        case ESPNOW_BUTTON1_LONG:
            color.green = 64,
            set_led(true, color);
            break;
        case ESPNOW_BUTTON2_SHORT:
            color.blue = 64,
            set_led(true, color);
            break;
        case ESPNOW_BUTTON2_LONG:
            set_led(false, color);
            break;
        default:
            ESP_LOGE(TAG, "Unhandled espnow event %d encountered", event->type);
    }
}

static void onEvent(const WlswEvent_t* event) {
    switch (event->type) {
        case WLSW_BUTTON_PRESS:
            ESP_LOGI(TAG, "Handle btn press event type %d", event->data.btn_event.event);
            handle_btn_press(&event->data.btn_event);
            break;
        case WLSW_ESPNOW_MSG_RCV:
            ESP_LOGI(TAG, "Handle espnow event type %d", event->data.espnow_event.type);
            handle_espnow_rcv(&event->data.espnow_event);
            break;
        default:
            ESP_LOGE(TAG, "Unhandled event %d encountered", event->type);
    }
}


static void main_task(void *pvParameter)
{
    WlswEvent_t event;
    ESP_LOGI(TAG, "Start of main task");
    while (!mMainTaskQuit) {

        BaseType_t eventReceived = xQueueReceive(queue, &event, pdMS_TO_TICKS(10000));

        if (eventReceived == pdTRUE) {
            ESP_LOGI(TAG, "onEvent");
            onEvent(&event);
        } else {
            ESP_LOGI(TAG, "Alive");
        }
    }
}

void notify_button_event(button_event_t* btn_event) {
    ESP_LOGI(TAG, "Send btn event %d", btn_event->event);
    /*
    WlswEvent_t event = {
        .type = WLSW_BUTTON_PRESS,
        .data.btn_event.pin = btn_event->pin,
        .data.btn_event.event = btn_event->event,
    };
    xQueueSend(queue, &event, portMAX_DELAY);
    */
    devent.type = WLSW_BUTTON_PRESS;
    devent.data.btn_event.pin = btn_event->pin;
    devent.data.btn_event.event = btn_event->event;

    xQueueSend(queue, &devent, portMAX_DELAY);
}


void notify_espnow_event(espnow_mevent_t* espnow_event) {
    ESP_LOGI(TAG, "Send espnow event %d", espnow_event->type);
    /*
    WlswEvent_t event = {
        .type = WLSW_ESPNOW_MSG_RCV,
        .data.espnow_event.type = espnow_event->type,
    };
    BaseType_t ret = xQueueSend(queue, &event, portMAX_DELAY);
    */

    devent.type = WLSW_ESPNOW_MSG_RCV;
    devent.data.espnow_event.type = espnow_event->type;
    xQueueSend(queue, &devent, portMAX_DELAY);
}


esp_err_t wslw_init()
{
    // Initialize global state and queue
    queue = xQueueCreate(10, sizeof(WlswEvent_t));
    if (NULL == queue) {
        ESP_LOGE(TAG, "Creating queue failed");
        return ESP_FAIL;
    }

    // Spawn a task to monitor the pins
    xTaskCreate(&main_task, "main_task", 4096, NULL, 8, NULL);

    return ESP_OK;
}
