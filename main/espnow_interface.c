/* ESPNOW Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/

/*
   This example shows how to use ESPNOW.
   Prepare two device, one for sending ESPNOW data and another for receiving
   ESPNOW data.
*/
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <assert.h>
#include "freertos/FreeRTOS.h"
#include "freertos/semphr.h"
#include "freertos/timers.h"
#include "nvs_flash.h"
#include "esp_random.h"
#include "esp_event.h"
#include "esp_netif.h"
#include "esp_log.h"
#include "esp_mac.h"
#include "esp_crc.h"
#include "espnow_interface.h"
#include "light_ctrl.h"
#include "wlsw.h"

#define ESPNOW_MAXDELAY 512

// static uint8_t mac_1[ESP_NOW_ETH_ALEN] = {0x7c, 0xdf, 0xa1, 0xba, 0x9a, 0x24};
// static uint8_t mac_2[ESP_NOW_ETH_ALEN] = {0xf4, 0x12, 0xfa, 0xfa, 0x1e, 0x34};

static const char *TAG = "espnow_interface";

static QueueHandle_t espnow_queue;

static uint8_t s_example_broadcast_mac[ESP_NOW_ETH_ALEN] = { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
static uint16_t s_espnow_seq[ESPNOW_DATA_MAX] = { 0, 0 };

static void espnow_deinit(example_espnow_send_param_t *send_param);


/* ESPNOW sending or receiving callback function is called in WiFi task.
 * Users should not do lengthy operations from this task. Instead, post
 * necessary data to a queue and handle it from a lower priority task. */
static void espnow_send_cb(const uint8_t *mac_addr, esp_now_send_status_t status)
{
    espnow_event_t evt;
    espnow_event_send_cb_t *send_cb = &evt.info.send_cb;

    if (mac_addr == NULL) {
        ESP_LOGE(TAG, "Send cb arg error");
        return;
    }

    evt.id = ESPNOW_SEND_CB;
    memcpy(send_cb->mac_addr, mac_addr, ESP_NOW_ETH_ALEN);
    send_cb->status = status;
    if (xQueueSend(espnow_queue, &evt, ESPNOW_MAXDELAY) != pdTRUE) {
        ESP_LOGW(TAG, "Send send queue fail");
    }
}

static void espnow_recv_cb(const uint8_t *mac_addr, const uint8_t *data, int len)
{
    espnow_event_t evt;
    espnow_event_recv_cb_t *recv_cb = &evt.info.recv_cb;

    if (mac_addr == NULL || data == NULL || len <= 0) {
        ESP_LOGE(TAG, "Receive cb arg error");
        return;
    }

    evt.id = ESPNOW_RECV_CB;
    memcpy(recv_cb->mac_addr, mac_addr, ESP_NOW_ETH_ALEN);
    recv_cb->data = malloc(len);
    if (recv_cb->data == NULL) {
        ESP_LOGE(TAG, "Malloc receive data fail");
        return;
    }
    memcpy(recv_cb->data, data, len);
    recv_cb->data_len = len;
    if (xQueueSend(espnow_queue, &evt, ESPNOW_MAXDELAY) != pdTRUE) {
        ESP_LOGW(TAG, "Send receive queue fail");
        free(recv_cb->data);
    }
}

/* Parse received ESPNOW data. */
int example_espnow_data_parse(uint8_t *data, uint16_t data_len, uint8_t *state, uint16_t *seq, int *magic)
{
    example_espnow_data_t *buf = (example_espnow_data_t *)data;
    uint16_t crc, crc_cal = 0;

    if (data_len < sizeof(example_espnow_data_t)) {
        ESP_LOGE(TAG, "Receive ESPNOW data too short, len:%d", data_len);
        return -1;
    }

    *state = buf->state;
    *seq = buf->seq_num;
    *magic = buf->magic;
    crc = buf->crc;
    buf->crc = 0;
    crc_cal = esp_crc16_le(UINT16_MAX, (uint8_t const *)buf, data_len);

    if (crc_cal == crc) {
        return buf->type;
    }

    return -1;
}

/* Prepare ESPNOW data to be sent. */
void example_espnow_data_prepare(example_espnow_send_param_t *send_param)
{
    example_espnow_data_t *buf = (example_espnow_data_t *)send_param->buffer;

    assert(send_param->len >= sizeof(example_espnow_data_t));

    buf->type = IS_BROADCAST_ADDR(send_param->dest_mac) ? ESPNOW_DATA_BROADCAST : ESPNOW_DATA_UNICAST;
    buf->state = send_param->state;
    buf->seq_num = s_espnow_seq[buf->type]++;
    buf->crc = 0;
    buf->magic = send_param->magic;
    /* Fill all remaining bytes after the data with random values */
    esp_fill_random(buf->payload, send_param->len - sizeof(example_espnow_data_t));
    buf->crc = esp_crc16_le(UINT16_MAX, (uint8_t const *)buf, send_param->len);
    ESP_LOGI(TAG, "crc: 0x%04x", buf->crc);
}


example_espnow_send_param_t *send_param;

static void espnow_handle_rcv_message(const uint8_t *mac_addr, uint8_t *data, int data_len)
{
    for (int i=0; i<data_len; ++i){
        ESP_LOGI(TAG, "0x%02x", data[i]);
    }

    espnow_data_t *buf = (espnow_data_t *)data;
    uint16_t crc, crc_cal = 0;

    if (data_len < sizeof(espnow_data_t)) {
        ESP_LOGE(TAG, "Receive ESPNOW data too short, len:%d", data_len);
        return;
    }

    crc = buf->crc;
    buf->crc = 0;
    crc_cal = esp_crc16_le(UINT16_MAX, (uint8_t const *)buf, data_len);

    if (crc_cal != crc) {
        ESP_LOGI(TAG, "Received invalid msg (bad crc)");
        return;
    }
    
    ESP_LOGI(TAG, "Received valid msg: # %d, magic: %lx, type: %d", buf->seq_num, buf->magic, buf->type);

    if (buf->type >= ESPNOW_TYPE_COUNT) {
        ESP_LOGI(TAG, "Unhandled type %d encountered.", buf->type);
        return;
    }

    espnow_mevent_t event = {.type = buf->type};
    notify_espnow_event(&event);
    ESP_LOGI(TAG, "NOTIFIED");

    // ret = example_espnow_data_parse(data, data_len, &recv_state, &recv_seq, &recv_magic);
    // free(data);
    // if (ret == ESPNOW_DATA_BROADCAST) {
    //     ESP_LOGI(TAG, "Receive %dth broadcast data from: "MACSTR", data_len: %d", recv_seq, MAC2STR(mac_addr), data_len);

    //     /* If MAC address does not exist in peer list, add it to peer list. */
    //     if (esp_now_is_peer_exist(mac_addr) == false) {
    //         esp_now_peer_info_t *peer = malloc(sizeof(esp_now_peer_info_t));
    //         if (peer == NULL) {
    //             ESP_LOGE(TAG, "Malloc peer information fail");
    //             espnow_deinit(send_param);
    //             vTaskDelete(NULL);
    //         }
    //         memset(peer, 0, sizeof(esp_now_peer_info_t));
    //         peer->channel = CONFIG_ESPNOW_CHANNEL;
    //         peer->ifidx = ESPNOW_WIFI_IF;
    //         peer->encrypt = true;
    //         memcpy(peer->lmk, CONFIG_ESPNOW_LMK, ESP_NOW_KEY_LEN);
    //         memcpy(peer->peer_addr, mac_addr, ESP_NOW_ETH_ALEN);
    //         ESP_ERROR_CHECK( esp_now_add_peer(peer) );
    //         free(peer);
    //     }

    //     if (send_param->state == 0) {
    //         send_param->state = 1;
    //     }

    //     /* If receive broadcast ESPNOW data which indicates that the other device has received
    //      * broadcast ESPNOW data and the local magic number is bigger than that in the received
    //      * broadcast ESPNOW data, stop sending broadcast ESPNOW data and start sending unicast
    //      * ESPNOW data.
    //      */
    //     if (recv_state == 1) {
    //         /* The device which has the bigger magic number sends ESPNOW data, the other one
    //          * receives ESPNOW data.
    //          */
    //         if (send_param->unicast == false && send_param->magic >= recv_magic) {
    //     	    ESP_LOGI(TAG, "Start sending unicast data");
    //     	    ESP_LOGI(TAG, "send data to "MACSTR"", MAC2STR(mac_addr));

    //     	    /* Start sending unicast ESPNOW data. */
    //             memcpy(send_param->dest_mac, mac_addr, ESP_NOW_ETH_ALEN);
    //             example_espnow_data_prepare(send_param);
    //             if (esp_now_send(send_param->dest_mac, send_param->buffer, send_param->len) != ESP_OK) {
    //                 ESP_LOGE(TAG, "Send error");
    //                 espnow_deinit(send_param);
    //                 vTaskDelete(NULL);
    //             }
    //             else {
    //                 send_param->broadcast = false;
    //                 send_param->unicast = true;
    //             }
    //         }
    //     }
    // }
    // else if (ret == ESPNOW_DATA_UNICAST) {
    //     ESP_LOGI(TAG, "Receive %dth unicast data from: "MACSTR" [len: %d]", recv_seq, MAC2STR(mac_addr), len);

    //     /* If receive unicast ESPNOW data, also stop sending broadcast ESPNOW data. */
    //     send_param->broadcast = false;
    // }
    // else {
    //     ESP_LOGI(TAG, "Receive error data from: "MACSTR"", MAC2STR(mac_addr));
    // }
}


static void espnow_handle_send_cmplt_message(const uint8_t *mac_addr, esp_now_send_status_t status)
{
    bool is_broadcast = IS_BROADCAST_ADDR(mac_addr);

    ESP_LOGD(TAG, "Send data to "MACSTR", status1: %d", MAC2STR(mac_addr), status);

    if (is_broadcast && (send_param->broadcast == false)) {
        return;
    }

    // if (!is_broadcast) {
    //     send_param->count--;
    //     if (send_param->count == 0) {
    //         ESP_LOGI(TAG, "Send done");
    //         espnow_deinit(send_param);
    //         vTaskDelete(NULL);
    //     }
    // }

    /* Delay a while before sending the next data. */
    // if (send_param->delay > 0) {
    //     vTaskDelay(send_param->delay/portTICK_PERIOD_MS);
    // }
}

static void espnow_task(void *pvParameter)
{
    espnow_event_t evt;
    send_param = (example_espnow_send_param_t *)pvParameter;

    while (1) {
        BaseType_t eventReceived = xQueueReceive(espnow_queue, &evt, pdMS_TO_TICKS(1000));
        if (eventReceived == pdTRUE) {
            switch (evt.id) {
                case ESPNOW_SEND_CB:
                {
                    espnow_event_send_cb_t *send_cb = &evt.info.send_cb;
                    espnow_handle_send_cmplt_message(send_cb->mac_addr, send_cb->status);
                    break;
                }
                case ESPNOW_RECV_CB:
                {
                    espnow_event_recv_cb_t *recv_cb = &evt.info.recv_cb;
                    espnow_handle_rcv_message(recv_cb->mac_addr, recv_cb->data, recv_cb->data_len);
                    break;
                }
                default:
                    ESP_LOGE(TAG, "Callback type error: %d", evt.id);
            }
        }
    }
}

esp_err_t espnow_init(void)
{
    example_espnow_send_param_t *send_param;

    espnow_queue = xQueueCreate(ESPNOW_QUEUE_SIZE, sizeof(espnow_event_t));
    if (espnow_queue == NULL) {
        ESP_LOGE(TAG, "Create mutex fail");
        return ESP_FAIL;
    }

    /* Initialize ESPNOW and register sending and receiving callback function. */
    ESP_ERROR_CHECK( esp_now_init() );
    ESP_ERROR_CHECK( esp_now_register_send_cb(espnow_send_cb) );
    ESP_ERROR_CHECK( esp_now_register_recv_cb(espnow_recv_cb) );
#if CONFIG_ESP_WIFI_STA_DISCONNECTED_PM_ENABLE
    ESP_ERROR_CHECK( esp_now_set_wake_window(65535) );
#endif
    /* Set primary master key. */
    ESP_ERROR_CHECK( esp_now_set_pmk((uint8_t *)CONFIG_ESPNOW_PMK) );

    /* Add broadcast peer information to peer list. */
    esp_now_peer_info_t *peer = malloc(sizeof(esp_now_peer_info_t));
    if (peer == NULL) {
        ESP_LOGE(TAG, "Malloc peer information fail");
        vSemaphoreDelete(espnow_queue);
        esp_now_deinit();
        return ESP_FAIL;
    }
    memset(peer, 0, sizeof(esp_now_peer_info_t));
    peer->channel = CONFIG_ESPNOW_CHANNEL;
    peer->ifidx = ESPNOW_WIFI_IF;
    peer->encrypt = false;
    memcpy(peer->peer_addr, s_example_broadcast_mac, ESP_NOW_ETH_ALEN);
    ESP_ERROR_CHECK( esp_now_add_peer(peer) );
    free(peer);

    /* Initialize sending parameters. */
    send_param = malloc(sizeof(example_espnow_send_param_t));
    if (send_param == NULL) {
        ESP_LOGE(TAG, "Malloc send parameter fail");
        vSemaphoreDelete(espnow_queue);
        esp_now_deinit();
        return ESP_FAIL;
    }
    memset(send_param, 0, sizeof(example_espnow_send_param_t));
    send_param->unicast = false;
    send_param->broadcast = true;
    send_param->state = 0;
    send_param->magic = esp_random();
    send_param->count = CONFIG_ESPNOW_SEND_COUNT;
    send_param->delay = CONFIG_ESPNOW_SEND_DELAY;
    send_param->len = CONFIG_ESPNOW_SEND_LEN;
    send_param->buffer = malloc(CONFIG_ESPNOW_SEND_LEN);
    if (send_param->buffer == NULL) {
        ESP_LOGE(TAG, "Malloc send buffer fail");
        free(send_param);
        vSemaphoreDelete(espnow_queue);
        esp_now_deinit();
        return ESP_FAIL;
    }
    memcpy(send_param->dest_mac, s_example_broadcast_mac, ESP_NOW_ETH_ALEN);
    example_espnow_data_prepare(send_param);

    ESP_LOGI(TAG, "magic: 0x%08lx", send_param->magic);

    xTaskCreate(espnow_task, "espnow_task", 2048, send_param, 4, NULL);

    return ESP_OK;
}

static void espnow_deinit(example_espnow_send_param_t *send_param)
{
    free(send_param->buffer);
    free(send_param);
    vSemaphoreDelete(espnow_queue);
    esp_now_deinit();
}

int32_t espnow_send_message(struct espnow_msg_t* msg)
{
    static uint8_t cnt = 0;
    ESP_LOGI(TAG, "preparing send");

    uint8_t buffer[256] = {};

    espnow_data_t* data_ptr = (espnow_data_t*)buffer;
    data_ptr->seq_num = cnt++;
    data_ptr->magic = 0xDEADBEEE;
    data_ptr->crc = 0;
    data_ptr->type = msg->type;
    data_ptr->len = 10 + msg->length;
    memcpy(data_ptr->payload, msg->data_ptr, msg->length);
    ESP_LOGI(TAG, "calc crc");

    /* Fill all remaining bytes after the data with random values */
    // esp_fill_random(buf->payload, send_param->len - sizeof(example_espnow_data_t));
    data_ptr->crc = esp_crc16_le(UINT16_MAX, (uint8_t const *)data_ptr, data_ptr->len);
    ESP_LOGI(TAG, "crc: 0x%04x", data_ptr->crc);

    /* Send the next data after the previous data is sent. */
    if (esp_now_send(s_example_broadcast_mac, buffer, data_ptr->len) != ESP_OK) {
        ESP_LOGE(TAG, "Send error");
        espnow_deinit(send_param);
        vTaskDelete(NULL);
    }
    return ESP_OK;
}
