#include "driver/gpio.h"
#include "led_strip.h"
#include "sdkconfig.h"
#include "esp_log.h"
#include "light_ctrl.h"

static const char *TAG = "light";

static led_strip_handle_t led_strip;


void set_led(bool on, struct rgb_t color) {
    if (on) {
        ESP_LOGI(TAG, "Set color to (%u, %u, %u)", color.red, color.green, color.blue);
        led_strip_set_pixel(led_strip, 0, color.red, color.green, color.blue);
        /* Refresh the strip to send data */
        led_strip_refresh(led_strip);
    } else {
        /* Set all LED off to clear all pixels */
        ESP_LOGI(TAG, "Switching LED off");
        led_strip_clear(led_strip);
    }
}

void configure_led(void)
{
    ESP_LOGI(TAG, "Example configured to blink addressable LED!");
    /* LED strip initialization with the GPIO and pixels number*/
    led_strip_config_t strip_config = {
        .strip_gpio_num = CONFIG_LED_GPIO,
        .max_leds = 1, // at least one LED on board
    };
    led_strip_rmt_config_t rmt_config = {
        .resolution_hz = 10 * 1000 * 1000, // 10MHz
    };
    ESP_ERROR_CHECK(led_strip_new_rmt_device(&strip_config, &rmt_config, &led_strip));
    /* Set all LED off to clear all pixels */
    led_strip_clear(led_strip);
}
